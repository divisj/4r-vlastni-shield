const int LEDS[] = { D5, D7 };

void LEDsSetup() {
  pinMode(LEDS[0], OUTPUT);
  pinMode(LEDS[1], OUTPUT);
}
void Red() {
  digitalWrite(LEDS[0], HIGH);
  digitalWrite(LEDS[1], LOW);
}

void Green() {
  digitalWrite(LEDS[0], LOW);
  digitalWrite(LEDS[1], HIGH);
}