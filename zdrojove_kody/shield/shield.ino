#include "leds.h"
#include "display.h"
#include "temp-sensor.h"
#include "rgb-strip.h"
#include "photo.h"

void setup() {
  LEDsSetup();
  displaySetup();
  rgbWS.begin();
  tempSetup();
  photoSetup();
}

void loop() {
  displayPhotoValue();
  delay(1000);
  displayFahrenheit();
  delay(1000);
  displayCelsius();
  delay(1000);
  delay(1000);
  Red();
  delay(500);
  Green();
  delay(500);
  setAll(255, 0, 0);
  delay(500);
  setAll(0, 255, 0);
  delay(500);
  setAll(0, 0, 255);
  delay(500);
  for (int i = 0; i < 200; i++) {
    betterChaos();
    delay(50);
  }
  delay(1000);
}

void displayCelsius() {
  lcd.init();
  // Call sensors.requestTemperatures() to issue a global temperature and Requests to all devices on the bus
  sensors.requestTemperatures();
  lcd.setCursor(0, 0);
  // Print a message to the LCD.
  lcd.print("Celsius temp: ");
  lcd.setCursor(0, 1);
  lcd.print(sensors.getTempCByIndex(0));
}
void displayFahrenheit() {
  lcd.init();
  sensors.requestTemperatures();
  lcd.clear();
  lcd.setCursor(0, 0);
  // Print a message to the LCD.
  lcd.print("Fahrenheit temp: ");
  lcd.setCursor(0, 1);
  lcd.print(sensors.getTempFByIndex(0));
}

void displayPhotoValue() {
  lcd.init();
  lcd.setCursor(0, 0);
  lcd.print("Photoresistor: ");
  lcd.setCursor(0, 1);
  lcd.print(getPhoto());
}
