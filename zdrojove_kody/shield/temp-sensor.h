#include <DallasTemperature.h>
#include <OneWire.h>

// Data wire is conntec to the Arduino digital pin 4
#define ONE_WIRE_BUS D10

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

void tempSetup() {
  Serial.begin(9600);
  // Start up the library
  sensors.begin();
}