# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 25 hodin |
| jak se mi to podařilo rozplánovat | podařilo|
| design zapojení | *URL obrázku designu* |
| proč jsem zvolil tento design | byl jednoduchý|
| zapojení | *URL obrázku zapojení* |
| z jakých součástí se zapojení skládá | deska, kabely, led pásek, teploměr, displej, fotorezistor, rezistor|
| realizace | *URL obrázku hotového produktu* |
| nápad, v jakém produktu vše propojit dohromady| nic mě nenapadá |
| co se mi povedlo | snad všechno |
| co se mi nepovedlo/příště bych udělal/a jinak | asi nic |
| zhodnocení celé tvorby | 9/10 |